# scripts

## autosplit-supertux

Autosplitter for speedrunning SuperTux. Simulates a key press every time you
finish a level. Works on GNU/Linux.

### How to use

1. Install dependencies:
   - inotify-tools
   - xdotool (if you use X)
   - [ydotool](https://github.com/ReimuNotMoe/ydotool/) (if you use Wayland)

2. Modify the source code if you want a different key than `F1`.
3. Set the environment variable `ST_SPEEDRUN_FILE` to the save file of the
   world that you run, e.g. to run Antarctica add the following line to your
   `~/.profile`:

        export ST_SPEEDRUN_FILE=.local/share/supertux2/profile2/world1.stsg

4. Make executable:

        chmod +x autosplit-supertux

5. Run the script:

        ./autosplit-supertux

6. Start your speedrun timer and SuperTux with the profile you specified in
   `ST_SPEEDRUN_FILE`. You have to do the first split manually. After a run,
   wipe out your save file:

        : > "$ST_SPEEDRUN_FILE"

### How does it work?

Every time you start or finish a level SuperTux updates the save file. This
script checks whenever a program writes to the file if the number of cleared
levels has increased.

## remind

Sends a notification after some time.

### Dependencies

- notify (script in this repo)
- systemd or sleep
- libpulse (optional for sound)
