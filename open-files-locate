#!/bin/sh

usage="open-files-locate <pattern>

Search disk using locate -i for pattern. Then select a file from the
results with fzf and open it with xdg-open. Use Ctrl-p to go back in fzf search
history. If locate yields only one result, it will be opened immedietly."

if [ $# -eq 0 ]; then
	>&2 echo "$usage"
	exit 2
fi

exclude="^$HOME/.cache/|^$HOME/.local/share/kak/|^$HOME/.local/share/nvim/"
choice="$(locate -i --all -- "$@" | grep -Ev "$exclude")"

if [ "$(echo "$choice" | wc -l)" -gt 1 ]; then
	choice="$(echo "$choice" | fzf --history "$HOME"/.local/share/fzf/history)"
	[ $? -eq 130 ] && exit 130
fi

if [ -n "$choice" ]; then
	xdg-open "$choice"
else
	>&2 echo "Not found"; exit 1
fi
